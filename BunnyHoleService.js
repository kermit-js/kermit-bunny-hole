'use strict';

const
  Service = require('kermit/Service'),
  BunnyHole = require('@gfcc/bunny-hole');

class BunnyHoleService extends Service {
  /**
   * Try to load config from environment variables.
   *
   * @returns {Object}
   */
  getDefaultServiceConfig() {
    let
      env = process.env,
      config = {},
      envMapping = {
        BH_AMQP_HOST: 'host',
        BH_AMQP_PORT: 'port',
        BH_AMQP_USER: 'username',
        BH_AMQP_PASS: 'password',
        BH_AMQP_EXCHANGE: 'eventExchangeName'
      };

    for (let envName in envMapping) {
      if (env[envName]) {
        config[envMapping[envName]] = env[envName];
      }
    }

    return {
      amqp: config
    };
  }

  /**
   * Return the bunny hole instance.
   *
   * @returns {BunnyHole}
   */
  getBunnyHole() {
    return this.bunnyHole;
  }

  /**
   * Fetch the bunny-hole AMQP config and the logger if configured.
   *
   * @returns {BunnyHoleService}
   */
  bootstrap() {
    let logger = this.serviceConfig.get('logger');
    this._amqpConfig = this.serviceConfig.get('amqp', {});

    if (logger) {
      // try to fetch the logging service in strict mode and fail if the given service does not exist.
      this._logger = this.serviceManager.get(logger, true);
    }

    return this;
  }

  /**
   * Initialize the bunny-hole and proxy the bunny-hole events.
   *
   * @returns {BunnyHoleService}
   */
  launch() {
    this.bunnyHole = new BunnyHole(
      this._amqpConfig,
      this._logger
    );

    this.bunnyHole.on('ready', () => {
      this.emit('ready');
    });

    this.bunnyHole.on('error', (msg) => {
      this.emit('error', msg);
    });

    return this;
  }

  /**
   * Call an rpc endpoint on the message queue.
   *
   * @param {string} name
   * @param {Object} params
   * @param {onCall} [callback]
   * @returns {BunnyHoleService}
   */
  call(name, params, callback) {
    this.bunnyHole.call(name, params, callback);

    return this;
  }


  /**
   * Emits an event into the message queue.
   *
   * @param {string} eventName
   * @param {Object} data
   * @returns {AmqpEventBus}
   */
  emitEvent(eventName, data) {
    this.bunnyHole.emitEvent(eventName, data);

    return this;
  }

  /**
   * Expose an rpc endpoint to the message queue.
   *
   * @param {string} name
   * @param {Function} onCall
   * @param {Object} [expectedParamsSchema]
   * @returns {BunnyHoleService}
   */
  expose(name, onCall, expectedParamsSchema) {
    this.bunnyHole.expose(name, onCall, expectedParamsSchema);

    return this;
  }

  /**
   * Registers a listener for an event on the message queue.
   *
   * @param {string} eventName
   * @param {Function} callback - Will be called when event was received
   * @param {string} [groupName] - Listener group name.
   *                               Will be used to create queue name so that multiple clients can connect to same queue.
   * @param {Object} [queueOptions] - Will be passed through to event queue. See https://www.npmjs.com/package/amqp#queue.
   * @returns {BunnyHoleService}
   */
  listenEvent(eventName, callback, groupName, queueOptions) {
    this.bunnyHole.listenEvent(eventName, callback, groupName, queueOptions);

    return this;
  }
}

module.exports = BunnyHoleService;